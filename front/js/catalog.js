'use strict';
function init() {
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  loadCatalog(1)
  let page = 1;
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  let loadMoreButton = document.getElementById("loadMore");
  loadMoreButton.addEventListener(`click`, () => {
    page++
    // момент с дизейблом кнопки надо делать через промис? Пока каталог не загрузился кнопка в дизейбле, после того как загрузился вкл?
    disableButtonLoadMore(loadMoreButton);
    loadCatalog(page)
    enableButtonLoadMore(loadMoreButton);
  })
}

function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
  let pointId = getPointId();
  api.getBikes(pointId, page).then(objBikes => {
    appendCatalog(objBikes.bikesList)
    showButtonLoadMore(objBikes.hasMore)
  })
}

function appendCatalog(bikesArray) {
  // отрисуй велосипеды из bikesArray в блоке <div id="bikeList">
  let bikeList = document.getElementById('bikeList');
  let ul = document.createElement(`ul`);
  ul.classList.add(`bike-cards-container`)

  let li = document.createElement(`li`);
  // 0: {name: "Велосипед Bulls Recreation Ground 1 2016", cost: 4, img: "vel5.jpg", _id: "1m5IBrrls785LLSM", isRented: false}
  bikeList.append(ul)
  bikesArray.forEach(bike => {
    let li = document.createElement(`li`);
    li.classList.add(`bike-info-container`);
    li.id = bike._id;
    ul.append(li);

    //картинка
    let divImg = document.createElement(`div`);
    divImg.classList.add(`bike-img`)
    let img = document.createElement(`img`);
    img.src = `/images/${bike.img}`;
    divImg.append(img);
    li.append(divImg);

    // название
    let titleDiv = document.createElement(`div`);
    titleDiv.innerHTML = bike.name;
    titleDiv.classList.add(`bike-name`);
    li.append(titleDiv)

    //стоимость
    let costDiv = document.createElement(`div`);
    costDiv.innerHTML = `Стоимость за час - ${bike.cost}`;
    costDiv.classList.add(`bike-cost`);
    li.append(costDiv)

    // кнопка арендовать
    let rentButton = document.createElement(`a`);
    rentButton.href = `/order/${bike._id}`;
    rentButton.innerHTML = `Арендовать`;
    rentButton.classList.add(`bike-rent-button`);
    if (bike.isRented) {
      rentButton.setAttribute(`disabled`, `disabled`);
    }
    rentButton.classList.add(`button`);
    li.append(rentButton)

  });



  //картинка
  let img = document.createElement(`img`);
  img.src = bikesArray[0].img;
  li.append(img);
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  let loadMoreButton = document.getElementById("loadMore");
  if (hasMore) {
    loadMoreButton.classList.remove(`hidden`);
    loadMoreButton.classList.add(`activeLoadMore`);
    enableButtonLoadMore(loadMoreButton);
  }
  else {
    loadMoreButton.classList.remove(`activeLoadMore`);
    loadMoreButton.classList.add(`hidden`);
    disableButtonLoadMore(loadMoreButton);
  }
  // иначе скрывай
}

function disableButtonLoadMore(button) {
  // заблокируй кнопку "загрузить еще"
  button.setAttribute(`disabled`, `disabled`);
}

function enableButtonLoadMore(button) {
  // разблокируй кнопку "загрузить еще"
  button.setAttribute(`disabled`, ``);
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  let url = new URL(document.URL).pathname.split("/");
  url = url[url.length - 1];
  return url;
}

document.addEventListener('DOMContentLoaded', init)
